FROM node:14.15.4-alpine

ENV PORT 3000
EXPOSE 3000

WORKDIR /usr/src/app

COPY package.json .
RUN npm i
COPY . .

CMD [ "node", "index.js" ]
