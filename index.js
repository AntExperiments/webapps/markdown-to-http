const express = require('express')
const fs = require('fs')
const md = require( "markdown" ).markdown

const app = express()
const port = 3000

const markdown = fs.readFileSync('data.md', 'utf8')

const html = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${md.parse(markdown)[1][2]}</title>
    <style> ${fs.readFileSync('style.css', 'utf8')} </style>
</head>
<body>
    ${md.toHTML(markdown)}
</body>
</html>
`

app.get(/./, (req, res) => {
    res.send(html)
})

app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
})
