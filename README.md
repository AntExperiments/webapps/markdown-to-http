# Markdown to HTTP

```yaml
  rentBoys:
      restart: always
      image: confusedant/markdown-to-http
      ports:
        - 3000:3000
      volumes:
        - "/my/important/file.md:/usr/src/app/data.md"
```